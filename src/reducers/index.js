import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router'
import Settings from './Settings';
import ToDo from './ToDo';
import Auth from './Auth';
import Common from "./Common";


export default (history) => combineReducers({
  router: connectRouter(history),
  settings: Settings,
  common: Common,
  toDo: ToDo,
  auth: Auth,
});
