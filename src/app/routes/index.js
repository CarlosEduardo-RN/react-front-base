import React from "react";
import { Route, Switch } from "react-router-dom";
import Dashboard from "./dashboard";
import Widgets from "./widgets";
import Components from "./components";
import AppModule from "./appModule";
import asyncComponent from "../../util/asyncComponent";
import { withRouter } from "react-router";
import Extensions from "./extensions";
import Chart from "./charts";

const Routes = ({ match }) =>
       <Switch>
              <Route path={`${match.url}/dashboard`} component={Dashboard} />
              <Route path={`${match.url}/widgets`} component={Widgets} />
              <Route path={`${match.url}/to-do-redux`}
                     component={asyncComponent(() => import("./todo/redux"))} />
              <Route path={`${match.url}/components`} component={Components} />
              <Route path={`${match.url}/app-module`} component={AppModule} />
              <Route path={`${match.url}/extensions`} component={Extensions} />
              <Route path={`${match.url}/chart`} component={Chart} />
              <Route component={asyncComponent(() => import("app/routes/extraPages/routes/404"))} />
       </Switch>;


export default withRouter(Routes);
