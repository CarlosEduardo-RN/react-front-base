import React from 'react';
import CustomScrollbars from 'util/CustomScrollbars';
import Navigation from "../../components/Navigation";

const SideBarContent = () => {
  const navigationMenus = [
    {
      name: 'sidebar.main',
      type: 'section',
      children: [
        {
          name: 'sidebar.dashboard',
          icon: 'view-dashboard',
          type: 'collapse',
          children: [
            {
              name: 'sidebar.dashboard.crm',
              type: 'item',
              link: '/app/dashboard/crm'
            },
          ]
        },
      ]
    },
    {
      name: 'sidebar.inBuiltApp',
      type: 'section',
      children: [
        {
          name: 'sidebar.appModule.mail',
          type: 'item',
          icon: 'email',
          link: '/app/mail'
        },
        {
          name: 'sidebar.appModule.toDo',
          type: 'item',
          icon: 'check-square',
          link: '/app/to-do'
        },
        {
          name: 'sidebar.appModule.contact',
          type: 'item',
          icon: 'account-box',
          link: '/app/contact'
        },
        {
          name: 'sidebar.appModule.chat',
          type: 'item',
          icon: 'comment',
          link: '/app/chat'
        }
      ]
    },
    {
      name: 'sidebar.view',
      type: 'section',
      children: [
        {
          name: 'sidebar.tables',
          icon: 'view-web',
          type: 'collapse',
          children: [
            {
              name: 'sidebar.tables.basicTable',
              type: 'item',
              link: '/app/table/basic'
            },
          ]
        },
      ]
    },
    {
      name: 'sidebar.form',
      type: 'section',
      children: [
        {
          name: 'sidebar.forms',
          icon: 'book-image',
          type: 'collapse',
          children: [
            {
              name: 'sidebar.forms.components',
              type: 'item',
              link: '/app/form/components'
            },
            {
              name: 'sidebar.forms.stepper',
              type: 'item',
              link: '/app/form/stepper'
            }
          ]
        }
      ]
    },
    {
      name: 'sidebar.extensions',
      type: 'section',
      children: [
        {
          name: 'sidebar.pickers',
          icon: 'brush',
          type: 'collapse',
          children: [
            {
              name: 'sidebar.pickers.dateTimePickers',
              type: 'item',
              link: '/app/pickers/date-time'
            },
          ]
        },
        {
          name: 'sidebar.extensions',
          icon: 'key',
          type: 'collapse',
          children: [
            {
              name: 'sidebar.extensions.sweetAlert',
              type: 'item',
              link: '/app/extensions/sweet-alert'
            },
            {
              name: 'sidebar.extensions.notification',
              type: 'item',
              link: '/app/extensions/notification'
            }
          ]
        },
        {
          name: 'sidebar.chart',
          icon: 'chart',
          type: 'collapse',
          children: [
            {
              name: 'sidebar.chart.line',
              type: 'item',
              link: '/app/chart/line'
            },
            {
              name: 'sidebar.chart.bar',
              type: 'item',
              link: '/app/chart/bar'
            },
            {
              name: 'sidebar.chart.area',
              type: 'item',
              link: '/app/chart/area'
            },
            {
              name: 'sidebar.chart.composed',
              type: 'item',
              link: '/app/chart/composed'
            },
            {
              name: 'sidebar.chart.scatter',
              type: 'item',
              link: '/app/chart/scatter'
            },
            {
              name: 'sidebar.chart.pie',
              type: 'item',
              link: '/app/chart/pie'
            },
            {
              name: 'sidebar.chart.radial',
              type: 'item',
              link: '/app/chart/radial'
            },
            {
              name: 'sidebar.chart.radar',
              type: 'item',
              link: '/app/chart/radar'
            },
            {
              name: 'sidebar.chart.tree',
              type: 'item',
              link: '/app/chart/treemap'
            }
          ]
        },
      ]
    },
  ];

  return (
    <CustomScrollbars className=" scrollbar">
      <Navigation menuItems={navigationMenus}/>
    </CustomScrollbars>
  );
};

export default SideBarContent;
