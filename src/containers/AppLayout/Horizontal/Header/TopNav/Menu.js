import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';

import IntlMessages from 'util/IntlMessages';


class Menu extends Component {

  componentDidMount() {
    const { history } = this.props;

    const pathname = `#${history.location.pathname}`;// get current path
    const mainMenu = document.getElementsByClassName('nav-item');
    for (let i = 0; i < mainMenu.length; i++) {
      mainMenu[i].onclick = function () {
        for (let j = 0; j < mainMenu.length; j++) {
          if (mainMenu[j].classList.contains('active')) {
            mainMenu[j].classList.remove('active')
          }
        }
        this.classList.toggle('active');
      }
    }
    const subMenuLi = document.getElementsByClassName('nav-arrow');
    for (let i = 0; i < subMenuLi.length; i++) {
      subMenuLi[i].onclick = function () {
        for (let j = 0; j < subMenuLi.length; j++) {
          if (subMenuLi[j].classList.contains('active')) {
            subMenuLi[j].classList.remove('active')
          }
        }
        this.classList.toggle('active');
      }
    }
    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('active');
      } else {
        this.closest(activeLi, 'li').classList.add('active');
      }
      const parentNav = this.closest(activeNav, '.nav-item');
      if (parentNav) {
        parentNav.classList.add('active');
      }

    } catch (e) {

    }

  }

  closest(el, selector) {
    try {
      let matchesFn;
      // find vendor prefix
      ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
        if (typeof document.body[fn] === 'function') {
          matchesFn = fn;
          return true;
        }
        return false;
      });

      let parent;

      // traverse parents
      while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
          return parent;
        }
        el = parent;
      }
    } catch (e) {

    }

    return null;
  }


  render() {
    return (
      <div className="app-main-menu d-none d-md-block">
        <ul className="navbar-nav navbar-nav-mega">

          <li className="nav-item">
            <span className="nav-link"><IntlMessages id="sidebar.main" /></span>
            <ul className="sub-menu">

              <li className="nav-arrow">
                <span className="nav-link">
                  <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
                  <span className="nav-text">
                    <IntlMessages id="sidebar.dashboard" />
                  </span>
                </span>
                <ul className="sub-menu">
                  <li>
                    <NavLink className="prepend-icon" to="/app/dashboard/crm">
                      <span className="nav-text"><IntlMessages id="sidebar.dashboard.crm" /></span>
                    </NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-arrow">
                <span className="nav-link">
                  <i className="zmdi zmdi-widgets zmdi-hc-fw" />
                  <span className="nav-text">
                    <IntlMessages id="sidebar.widgets" />
                  </span>
                </span>
                <ul className="sub-menu">
                  <li>
                    <NavLink className="prepend-icon" to="/app/widgets/modern">
                      <span className="nav-text"><IntlMessages id="sidebar.modern" /></span>
                    </NavLink>
                  </li>
                </ul>
              </li>

            </ul>
          </li>

          <li className="nav-item mega-menu">
            <span className="nav-link"><IntlMessages id="sidebar.components" /></span>

            <ul className="sub-menu">
              <li>
                <NavLink className="prepend-icon" to="/app/components/alerts">
                  <span className="nav-text"><IntlMessages id="sidebar.components.alerts" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/appbar">
                  <span className="nav-text"><IntlMessages id="sidebar.components.appbar" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/avatars">
                  <span className="nav-text"><IntlMessages id="sidebar.components.avatars" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/badges">
                  <span className="nav-text"><IntlMessages id="sidebar.components.badge" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/bottom-navigation">
                  <span className="nav-text"><IntlMessages
                    id="sidebar.components.bottomNavigation" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/breadcrumbs">
                  <span className="nav-text"><IntlMessages
                    id="sidebar.components.breadcrumbs" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/buttons">
                  <span className="nav-text"><IntlMessages id="sidebar.components.buttons" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/button-group">
                  <span className="nav-text"><IntlMessages
                    id="sidebar.components.buttonGroup" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/cards">
                  <span className="nav-text"><IntlMessages id="sidebar.components.cards" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/dialogs">
                  <span className="nav-text"><IntlMessages id="sidebar.components.dialogs" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/dividers">
                  <span className="nav-text"><IntlMessages id="sidebar.components.dividers" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/expansion-panel">
                  <span className="nav-text"><IntlMessages
                    id="sidebar.components.expansionPanel" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/pickers">
                  <span className="nav-text"><IntlMessages id="sidebar.components.pickers" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/progressbar">
                  <span className="nav-text"><IntlMessages id="sidebar.components.progress" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/selects">
                  <span className="nav-text"><IntlMessages id="sidebar.components.selects" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/selection">
                  <span className="nav-text"><IntlMessages
                    id="sidebar.components.selectionControl" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/snackbar">
                  <span className="nav-text"><IntlMessages id="sidebar.components.snackbars" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/stepper">
                  <span className="nav-text"><IntlMessages id="sidebar.components.stepper" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/components/tables">
                  <span className="nav-text"><IntlMessages id="sidebar.components.tables" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/text-fields">
                  <span className="nav-text"><IntlMessages id="sidebar.components.textFields" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/tooltips">
                  <span className="nav-text"><IntlMessages id="sidebar.components.tooltips" /></span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon"
                  to="/app/components/typography">
                  <span className="nav-text"><IntlMessages id="sidebar.components.typography" /></span>
                </NavLink>
              </li>
            </ul>

          </li>

          <li className="nav-item">
            <span className="nav-link"><IntlMessages id="sidebar.extensions" /></span>
            <ul className="sub-menu">

              <li className="nav-arrow">
                <span className="nav-link">
                  <i className="zmdi zmdi-key zmdi-hc-fw" />
                  <span className="nav-text"><IntlMessages id="sidebar.extensions" /></span>
                </span>

                <ul className="sub-menu">
                  <li>
                    <NavLink className="prepend-icon" to="/app/extensions/dropzone">
                      <span className="nav-text"><IntlMessages id="sidebar.extensions.dropzone" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/extensions/sweet-alert">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.extensions.sweetAlert" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/extensions/notification">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.extensions.notification" /></span>
                    </NavLink>
                  </li>
                </ul>

              </li>

              <li className="nav-arrow">
                <span className="nav-link">
                  <i className="zmdi zmdi-chart zmdi-hc-fw" />
                  <span className="nav-text"><IntlMessages id="sidebar.chart" /></span>
                </span>

                <ul className="sub-menu sub-menu-half">
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/line">
                      <span className="nav-text"><IntlMessages id="sidebar.chart.line" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/bar">
                      <span className="nav-text"><IntlMessages id="sidebar.chart.bar" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/area">
                      <span className="nav-text"><IntlMessages id="sidebar.chart.area" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/composed">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.chart.composed" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/scatter">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.chart.scatter" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/pie">
                      <span className="nav-text"><IntlMessages id="sidebar.chart.pie" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/radial">
                      <span className="nav-text"><IntlMessages id="sidebar.chart.radial" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/radar">
                      <span className="nav-text"><IntlMessages id="sidebar.chart.radar" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/chart/treemap">
                      <span className="nav-text"><IntlMessages id="sidebar.chart.tree" /></span>
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li className="nav-arrow">
                <span className="nav-link">
                  <i className="zmdi zmdi-collection-item-8 zmdi-hc-fw" />
                  <span className="nav-text"><IntlMessages id="sidebar.appModule" /></span>
                </span>

                <ul className="sub-menu sub-menu-half">
                  <li>
                    <NavLink className="prepend-icon" to="/app/app-module/login-1">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.login1" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon" to="/app/app-module/login-2">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.login2" /></span>
                    </NavLink>
                  </li>

                  <li>
                    <NavLink className="prepend-icon"
                      to="/app/app-module/login-with-stepper">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.loginStepper" /></span>
                    </NavLink>
                  </li>

                  <li>
                    <NavLink className="prepend-icon"
                      to="/app/app-module/sign-up-1">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.signup1" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon"
                      to="/app/app-module/sign-up-2">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.signup2" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon"
                      to="/app/app-module/forgot-password-1">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.forgotPassword1" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon"
                      to="/app/app-module/forgot-password-2">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.forgotPassword2" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon"
                      to="/app/app-module/lock-screen-1">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.lock1" /></span>
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="prepend-icon"
                      to="/app/app-module/lock-screen-2">
                      <span className="nav-text"><IntlMessages
                        id="sidebar.appModule.lock2" /></span>
                    </NavLink>
                  </li>
                </ul>
              </li>

            </ul>
          </li>

          <li className="nav-item">
            <span className="nav-link"><IntlMessages id="sidebar.modules" /></span>

            <ul className="sub-menu">

              <li>
                <NavLink to="/app/to-do-redux">
                  <i className="zmdi zmdi-check-square zmdi-hc-fw" />
                  <span className="nav-text"><IntlMessages id="sidebar.appModule.toDoRedux" /></span>
                </NavLink>
              </li>

            </ul>
          </li>

        </ul>
      </div>
    );
  }
}

export default withRouter(Menu);
